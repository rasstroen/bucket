<?php

require_once 'vendor/autoload.php';

$connectionParams = [
	'dbname' => (string) isset($connectionConfig['dbname']) ? $connectionConfig['dbname'] : 'bucket',
	'user' => (string) isset($connectionConfig['user']) ? $connectionConfig['user'] : 'root',
	'password' => (string) isset($connectionConfig['password']) ? $connectionConfig['password'] : '2912',
	'host' => (string) isset($connectionConfig['host']) ? $connectionConfig['host'] : 'localhost',
	'driver' => (string) isset($connectionConfig['driver']) ? $connectionConfig['driver'] : 'mysqli',
	'charset' => (string) isset($connectionConfig['charset']) ? $connectionConfig['charset'] : 'utf8',
];
$connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
if (!empty($connectionParams['charset'])) {
	$connection->query('SET NAMES ' . (string) $connectionParams['charset']);
}


$bll = new \Chubucket\BLL($connection);
$bucketManager = new \Chubucket\Manager($bll);
$i = 0;
while (true) {

	$i++;
	if ($bucketManager->putIntoBucket('192.168.0.1', 1000)) {
		echo "success {$i}\n";
	} else {
		echo "fail {$i}\n";
	}
	if ($i > 40) {
		break;
	}
}
