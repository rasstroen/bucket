<?php namespace Chubucket;

class Bucket
{
	const DEFAULT_BUCKET_LIFETIME = 60;
	const DEFAULT_BUCKET_MAXIMUM_RPS = 10;

	/**
	 * @var BLL
	 */
	private $bll;

	/**
	 * @var string
	 */
	private $key;

	/**
	 * @var float
	 */
	private $maximumRPS;

	/**
	 * @var bool
	 */
	protected $isClosed;

	/**
	 * @var
	 */
	protected $tokensCount;

	/**
	 * @return string
	 */
	public function getKey()
	{
		return $this->key;
	}

	/**
	 * @return mixed
	 */
	public function getTokensCount()
	{
		return $this->tokensCount;
	}


	/**
	 * @param BLL $bll
	 * @param string $uniqueKey
	 * @param float|null $maximumRPS
	 */
	public function __construct(BLL $bll, $uniqueKey, $maximumRPS = null)
	{
		$this->bll = $bll;
		$this->key = (string) $uniqueKey;
		$this->maximumRPS = $maximumRPS ? (float) $maximumRPS : self::DEFAULT_BUCKET_MAXIMUM_RPS;
	}

	/**
	 * @return bool
	 */
	public function addToken()
	{
		$this->processBucketStatus();
		return !$this->isClosed();
	}

	private function calculateBucketParameters(array $bucketStatus)
	{
		$this->maximumRPS;
		$this->tokensCount = $bucketStatus['tokens_count'] + 1;
		$waitTime = round(1 / $this->maximumRPS, 4);
		if ($bucketStatus['current_microtime'] - $bucketStatus['last_token_microtime'] < $waitTime) {
			$this->isClosed = true;
		} else {
			$this->isClosed = false;
		}
	}

	private function isClosed()
	{
		return $this->isClosed;
	}

	private function processBucketStatus()
	{
		$this->bll->startTransaction();
		$bucketStatus = $this->bll->getBucketStatusForUpdate($this->key);
		if (false === $bucketStatus) {
			$bucketStatus = $this->bll->createBucket($this->key);
		}
		$this->calculateBucketParameters($bucketStatus);
		if (!$this->isClosed()) {
			$this->updateBucketStatus();
		}
		$this->bll->commit();
	}

	private function updateBucketStatus()
	{
		$this->bll->updateBucketStatus($this);
	}
}