<?php namespace Chubucket;

class Manager
{
	/**
	 * @var BLL
	 */
	private $bll;

	/**
	 * @var Bucket[]
	 */
	private $buckets;

	public function __construct(BLL $bll)
	{
		$this->bll = $bll;
	}

	/**
	 * @param string $bucketUniqueKey
	 * @param float $maximumRPS
	 * @return bool
	 */
	public function putIntoBucket($bucketUniqueKey, $maximumRPS = null)
	{
		if (!isset($this->buckets[$bucketUniqueKey])) {
			$this->buckets[$bucketUniqueKey] = new Bucket(
				$this->bll,
				$bucketUniqueKey,
				$maximumRPS
			);
		}

		$result = $this->buckets[$bucketUniqueKey]->addToken();
		return $result;
	}
}