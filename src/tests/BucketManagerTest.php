<?php namespace Chubucket\tests;

use Chubucket\BLL;
use Chubucket\Manager;

class BucketManagerTest extends \PHPUnit_Framework_TestCase
{
	public function testAddTokenToEmptyBucket()
	{
		$bucketUniqueKey = 'some_ip_or_host';
		$maximumRPS = 10;
		$lifeTime = 60;


		$bll = $this->prophesize(BLL::class);
		$bll->startTransaction()->shouldBeCalled();
		$bll->getBucketStatusForUpdate($bucketUniqueKey)->willReturn([]);
		$bll->commit()->shouldBeCalled();


		$bucketManager = new Manager($bll->reveal());

		$allowed = $bucketManager->putIntoBucket(
			$bucketUniqueKey,
			$maximumRPS,
			$lifeTime
		);
		$this->assertTrue($allowed);
	}
}