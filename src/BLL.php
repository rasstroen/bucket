<?php namespace Chubucket;

use Doctrine\DBAL\Types\Type;

class BLL
{
	/**
	 * @var \Doctrine\DBAL\Connection
	 */
	private $connection;

	public function __construct($connection)
	{
		$this->connection = $connection;
	}

	public function startTransaction()
	{
		$this->connection->beginTransaction();
	}


	public function commit()
	{
		$this->connection->commit();
	}

	/**
	 * @param string $key
	 * @return array
	 */
	public function createBucket($key)
	{
		$bucketStatus = [
			'key' => $key,
			'tokens_count' => 0
		];

		$currentMicrotime = $this->connection->query('SELECT UNIX_TIMESTAMP(NOW(4)) as `current_microtime`')->fetch();

		$this->connection->executeQuery(
			'INSERT INTO bucket SET 
				 `key`=?,
				 `tokens_count`=?,
				 `create_microtime`=UNIX_TIMESTAMP(NOW(4)),
				 `last_token_microtime`=0
				',
			array_values($bucketStatus),
			[
				Type::STRING,
				Type::INTEGER,
				Type::INTEGER,
			]
		);
		$bucketStatus['current_microtime'] = $bucketStatus['create_microtime'] = $currentMicrotime['current_microtime'];
		return $bucketStatus;
	}

	public function updateBucketStatus(Bucket $bucket)
	{
		$updatedBucket = [
			$bucket->getTokensCount(),
			$bucket->getKey(),
		];
		$this->connection->executeQuery(
			'UPDATE `bucket` SET `tokens_count`=?,`last_token_microtime`=UNIX_TIMESTAMP(NOW(4)) WHERE `key`=?',
			$updatedBucket,
			[
				Type::INTEGER,
				Type::STRING
			]
		);
	}

	/**
	 * @param string $key
	 * @return array|false
	 */
	public function getBucketStatusForUpdate($key)
	{

		return $this->connection->fetchAssoc(
			'SELECT 
					`key`,
				 	`tokens_count`,
				 	`create_microtime`,
				 	`last_token_microtime`,
				 	UNIX_TIMESTAMP(NOW(4)) as `current_microtime`
				 FROM
				 	`bucket`
				 WHERE
				 	`key` = ?
				 FOR UPDATE',
			[
				$key
			],
			[
				Type::STRING
			]
		);
	}
}