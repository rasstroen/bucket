CREATE DATABASE IF NOT EXISTS bucket charset=utf8;

use bucket;

DROP TABLE IF EXISTS `bucket`;

CREATE TABLE `bucket` (
  `key` varchar(100) NOT NULL DEFAULT '',
  `tokens_count` int(11) DEFAULT NULL,
  `create_microtime` decimal(14,4) DEFAULT NULL,
  `last_token_microtime` decimal(14,4) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;